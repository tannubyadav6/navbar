import React, {useState} from 'react'
import styles from './ProFileIcon.module.css'
const ProFileIcon = () => {
  const [profileType, setProfileType] = useState('A')
  const handleProfileChange = () =>{
    setProfileType((prevState)=> {
      if(prevState === 'A') return 'B'
      else return 'A'
    })
  }
  return (
    <div className={`${styles.ProFileIconMain} ${profileType === 'A' ? styles.student: styles.teacher}`} onClick={handleProfileChange}>
      <img src="https://cdn3.iconfinder.com/data/icons/avatars-collection/256/47-512.png" alt="profile icon" className={styles.ProFileIconImage}/>
    <span className={`${styles.ProFileIconType}  ${profileType === 'A' ? styles.student: styles.teacher}`}>
      {profileType}
    </span>
    </div>
  )
}

export default ProFileIcon