import React from 'react'
import styles from './CourseCard.module.css'
import MoreVertIcon from '@mui/icons-material/MoreVert';
 export const CourseCard = (props) => {
  
  return (
    <div className= {`${styles.CourseCardMain}`}>
        <div style ={{backgroundImage:`url(${props.src})`}} className={`${styles.CourseCardIage}`}/>
    <span className={`${styles.CourseCardtitleArea}`}>
    <span className={`${styles.CourseCardtitle}`}> {props.title}</span> <MoreVertIcon />
    </span>
 
<span className={`${styles.CourseCardDescription}`}>
    {props.count} courses
</span>
    </div>
  )
}

