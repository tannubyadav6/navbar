import React,{useState} from 'react'
import SearchIcon from '@mui/icons-material/Search';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import styles from './NavBar.module.css';
import ProFileIcon from './ProFileIcon';
import MenuIcon from '@mui/icons-material/Menu';
const NavBar = () => {
  const[isSearchActive,setIsSearchActive]=useState(false);

  const handleSearchClick=() =>{
    setIsSearchActive(!isSearchActive);
  }

  return (
    <div className={styles.NavBarMain}>
    <MenuIcon className={`${styles.NavBarMenu}  ${isSearchActive ? styles.searchActive : ''}`}/>
    <div className={`${styles.NavBarLogo}  ${isSearchActive ? styles.searchActive : ''}`}/>
    <div  className={`${styles.NavBarSearchArea} ${isSearchActive ? styles.searchActive : ''}`}>
    <input type ="text" className={`${styles.NavBarSearchInput} ${isSearchActive ? styles.searchActive : ''}`} placeholder='what do you want to learn today?'/> 
      <button className={`${styles.NavBarSearchIcon} ${isSearchActive ? styles.searchActive : ''}`} onClick={handleSearchClick}>
        <SearchIcon/>
      </button>
      <button className={`${styles.NavBarCoursesButton} ${isSearchActive ? styles.searchActive : ''}`}>
        Courses 
      </button>
      <span className={`${styles.NavBarSrevicesButton} ${isSearchActive ? styles.searchActive : ''}`}>
        Our sevices <KeyboardArrowDownIcon />
      </span>
      <ProFileIcon/>
      </div>
    </div>
  )
}

export default NavBar